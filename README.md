💡목표

-오픈 API를 이용하여 강아지 슬라이드 웹페이지를 구현한다. 



📈 예상효과

-API의 쓰임을 알게된다.

-자바스크립트의 async-await 을 사용하는 법을 알게된다. 

-자바스크립트 비동기 로직을 이해한다.





🔗관련문서 

https://dog.ceo/dog-api/documentation/ 



💭 아키텍처 구성 및 접근방법 계획

1.강아지 종류를 선택할 수 있는 <select>를 이용하여 

API소스의 데이터를 async를 이용하여 받는다. 

2.받아온 데이터를 html로 구현한다.

 



