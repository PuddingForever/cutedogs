let timer
let deleteFirstPhotoDelay

//데이터 받기 
async function start() {
  try {
    const response = await fetch("https://dog.ceo/api/breeds/list/all")
    const data = await response.json()
    createBreedList(data.message)
  } catch (e) {
    console.log("서버 정보를 받아오지 못했습니다.")
  }
}

start();

//실제 html구현 
function createBreedList(breedList) {
  document.getElementById("breed").innerHTML = `
  <select onchange="loadByBreed(this.value)">
        <option>Choose a dog breed</option>
        ${Object.keys(breedList).map(function (breed) {
          return `<option>${breed}</option>`
        }).join('')}
      </select>
  `
}

//데이터 받기 
async function loadByBreed(breed) {
  if (breed != "강아지 종류를 선택하세요") {
    const response = await fetch(`https://dog.ceo/api/breed/${breed}/images`)
    const data = await response.json()
    createSlideshow(data.message)
  }
}

//실제 html구현하기 

function createSlideshow(images) {
  let currentPosition = 0
  clearInterval(timer)
  clearTimeout(deleteFirstPhotoDelay)
  
  //사진이 한장 이상이면 0부터 1까지 사진을 보여줄건데 css에서 opacity 0으로 해서 안보임
  if (images.length > 1) {
    document.getElementById("slideshow").innerHTML = `
  <div class="slide" style="background-image: url('${images[0]}')"></div>
  <div class="slide" style="background-image: url('${images[1]}')"></div>
  `
  currentPosition += 2;
  //정확히 2장만 있다면 currentPosition =2로 가면 안되기 때문에 설정해준다.  
  if (images.length == 2) currentPosition = 0
  //3초마다 nextSlide실행 
  timer = setInterval(nextSlide, 3000)
  } else {
    //사진이 한장밖에 없으면 , 한장만 보여주고 끝 . 
    document.getElementById("slideshow").innerHTML = `
  <div class="slide" style="background-image: url('${images[0]}')"></div>
  <div class="slide"></div>
  `
  }

  //currentposition 쓰기 위해 함수 안에 만든다
  function nextSlide() {
    //slideshow 제일 끝에 넣어줌 
    document.getElementById("slideshow").insertAdjacentHTML("beforeend", `<div class="slide" style="background-image: url('${images[currentPosition]}')"></div>`)
    deleteFirstPhotoDelay = setTimeout(function () {
      document.querySelector(".slide").remove()
    }, 1000)
    
    if (currentPosition + 1 >= images.length) {
      //마지막으로 가면 , 처음으로 돌아간다 
        currentPosition = 0;
    } else {
      currentPosition++;
    }
  }
}